# SCSS params
SCSS_DIR = ./static/scss
SCSS_FILE = style.scss

# CSS params
CSS_DIR = ./static/css
CSS_FILE = style.min.css

define build_scss
	./node_modules/node-sass/bin/node-sass --source-map true --source-map-embed true --output-style compressed $(SCSS_DIR)/$(1) > $(CSS_DIR)/$(CSS_FILE)
endef

.PHONY: clean demo build build-ltr

build: clean build-ltr

build-ltr:
	$(call build_scss,$(SCSS_FILE),$(CSS_FILE))

clean:
	rm -f $(CSS_DIR)/*.css
